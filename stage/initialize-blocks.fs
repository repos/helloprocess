\    This file is part of Hello Process.
\
\    Hello Process is free software: you can redistribute it and/or modify
\    it under the terms of the GNU General Public License as published by
\    the Free Software Foundation, either version 3 of the License, or
\    (at your option) any later version.
\
\    Hello Process is distributed in the hope that it will be useful,
\    but WITHOUT ANY WARRANTY; without even the implied warranty of
\    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\    GNU General Public License for more details.
\
\    You should have received a copy of the GNU General Public License
\    along with Hello Process.  If not, see <http://www.gnu.org/licenses/>.

\ This file initialises the process by randomly seeding x programs into x blocks

\ INDEX BLOCK used to identify blocks. variable 'target' is set to 0 for empty,
\ 1 for copycat, 2 for eraser head, etc. 
size @ 1 + l
: insert-line c/l * r# ! c/l len ! 0 parse tuck 'line insert IF update THEN v ;

: insert-index
0 insert-line s\"          (  0: empty)" 'line insert
1 insert-line s\" ( \1)    (  1: copy cat)" 'line insert
2 insert-line s\" ( \2)    (  2: eraser head)" 'line insert
3 insert-line s\" ( \5)    (  3: warlord)" 'line insert
4 insert-line s\" ( \20)   (  4: anti-eraser)" 'line insert
5 insert-line s\" ( \200)  (  5: 3-cell-jumper)" 'line insert
6 insert-line s\" ( \101)  (  6: 2-cell-jumper)" 'line insert
7 insert-line s\" ( \110)  (  7: square-pusher)" 'line insert
8 insert-line s\" ( \30)   (  8: swap-master)" 'line insert
9 insert-line s\" ( \10)   (  9: destroyer)" 'line insert
10 insert-line s\" ( \143) ( 10: back-swapper)" 'line insert
11 insert-line s\" ( \72)  ( 11: booby-trap)" 'line insert    
;
insert-index
update flush

\ The programs
: copy-cat
    b-empty?
    current-b @ l
    0 insert-line s\" ( \1)" 'line insert    
    1 insert-line s\"  .\" copy-cat \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  copy-ca" 'line insert
    write
;

: eraser-head
    b-empty?
    current-b @ l
    0 insert-line s\" ( \2)" 'line insert
    1 insert-line s\"  .\" eraser \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  eraser-ca" 'line insert
    write
;

: anti-emptiness-warlord
    b-empty?
    current-b @ l
    0 insert-line s\" ( \5)" 'line insert
    1 insert-line s\"  .\" warlord \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  warlord-ca" 'line insert
    write
;

: anti-eraser
    b-empty?
    current-b @ l
    0 insert-line s\" ( \20)" 'line insert
    1 insert-line s\"  .\" anti-eraser \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  anti-eraser-ca" 'line insert
    write
;

: 3-cell-jumper
    b-empty?
    current-b @ l
    0 insert-line s\" ( \200)" 'line insert
    1 insert-line s\"  .\" 3-cell-jumper \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  3-cell-ca" 'line insert
    write
;

: 2-cell-jumper
    b-empty?
    current-b @ l
    0 insert-line s\" ( \101)" 'line insert
    1 insert-line s\"  .\" 2-cell-jumper \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  2-cell-ca" 'line insert
    write
;

: square-pusher
    b-empty?
    current-b @ l
    0 insert-line s\" ( \110)" 'line insert
    1 insert-line s\"  .\" square-pusher \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  square-pusher-ca" 'line insert
    write
;

: swap-master
    b-empty?
    current-b @ l
    0 insert-line s\" ( \30)" 'line insert
    1 insert-line s\"  .\" swap-master \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  swap-master-ca" 'line insert
    write
;

: destroyer
    b-empty?
    current-b @ l
    0 insert-line s\" ( \10)" 'line insert
    1 insert-line s\"  .\" destroyer \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  destroyer-ca" 'line insert
    write
;

: back-swapper
    b-empty?
    current-b @ l
    0 insert-line s\" ( \143)" 'line insert
    1 insert-line s\"  .\" back-swapper \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  back-swapper-ca" 'line insert
    write
;

: booby-trap
    b-empty?
    current-b @ l
    0 insert-line s\" ( \72)" 'line insert
    1 insert-line s\"  .\" booby-trap \"" 'line insert
    2 insert-line S"  neighbour-check-new" 'line insert
    3 insert-line S"  booby-trap-ca" 'line insert
    write
;

\ words used to randomly seed a random number of programs each cycle
\ and those needed to print a statement at the beginning of each cycle
create start-text-3 512 allot
create start-text-4 512 allot
s\" A new file has been created.\nThe following programs have been seeded:\n!\131" start-text-3 place

: +txt start-text-3 +place ;
: ndrop 1 - for drop next ;
: ndup 1 - for dup next ;
: seed-amount total-programs @ random 3 + total-programs @ min ;
: write-me
    total-programs @ 1 - ndup
    0 = if copy-cat write 10 ndrop S" copycat " +txt else
	1 = if eraser-head write 9 ndrop S" eraser-head " +txt else
	    2 = if anti-emptiness-warlord write 8 ndrop S" warlord " +txt else
		3 = if anti-eraser write 7 ndrop S" anti-eraser " +txt else
		    4 = if 3-cell-jumper write 6 ndrop S" 3-cell-jumper " +txt else
			5 = if 2-cell-jumper write 5 ndrop S" 2-cell-jumper " +txt else
			    6 = if square-pusher write 4 ndrop S" square-pusher " +txt else
				7 = if swap-master write 3 ndrop S" swap-master " +txt else
				    8 = if destroyer write 2 ndrop S" destroyer " +txt else
					9 = if back-swapper write 1 ndrop S" back-swapper " +txt else
					    10 = if booby-trap write S" booby-trap " +txt
			then then then then then then then then then then then 
;  
: seed-who?
    seed-amount 1 -
    total-programs @ urn-init
    for
    urn-check
    write-me
    write
    next
    new-line
    new-line

    \ Print Header
    hello-process

    new-line
    start-text-3 count

    \ Printer code
    send-printer

    new-line
;

seed-who?
