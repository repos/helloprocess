\    This file is part of Hello Process.
\
\    Hello Process is free software: you can redistribute it and/or modify
\    it under the terms of the GNU General Public License as published by
\    the Free Software Foundation, either version 3 of the License, or
\    (at your option) any later version.
\
\    Hello Process is distributed in the hope that it will be useful,
\    but WITHOUT ANY WARRANTY; without even the implied warranty of
\    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\    GNU General Public License for more details.
\
\    You should have received a copy of the GNU General Public License
\    along with Hello Process.  If not, see <http://www.gnu.org/licenses/>.

\ dictionary.fs contains all the words used in blocks

variable prevc
variable nextc
variable 2prevc
variable 2nextc
variable target
variable overpop

: nothing ;
: write update flush ;

\ wrap makes sure targets are always within bounds
: wrap
    dup
    0 <=
    if
	size @ + 
    else
	dup size @ >
	if
	    size @ - abs
	then
    then
;
\ target=who? checks who is the target
: target=who?
    size @ 1 + block 64 + 4 target-b @ block 4 compare
    0 =
    if ." target=copycat " 1 target ! else
	size @ 1 + block 128 + 4 target-b @ block 4 compare
	0 =
	if ." target=eraser " 2 target ! else
	    size @ 1 + block 192 + 4 target-b @ block 4 compare
	    0 =
	    if ." target=warlord " 3 target ! else
		size @ 1 + block 256 + 4 target-b @ block 4 compare
		0 =
		if ." target=anti-eraser " 4 target ! else
		    size @ 1 + block 320 + 4 target-b @ block 4 compare
		    0 =
		    if ." target=3-cell-jumper " 5 target ! else
			size @ 1 + block 384 + 4 target-b @ block 4 compare
			0 =
			if 6 target ! ." target=2-cell-jumper " else
			    size @ 1 + block 448 + 4 target-b @ block 4 compare
			    0 =
			    if 7 target ! ." target=square-pusher " else
				size @ 1 + block 512 + 4 target-b @ block 4 compare
				0 =
				if 8 target ! ." swap-master " else
				    size @ 1 + block 576 + 4 target-b @ block 4 compare
				    0 =
				    if 9 target ! ." destroyer " else
					size @ 1 + block 640 + 4 target-b @ block 4 compare
					0 =
					if 10 target ! ." back-swapper " else
					    size @ 1 + block 704 + 4 target-b @ block 4 compare
					    0 =
					    if 11 target ! ." booby-trap " else
						    ." target=EMPTY " 0 target !
			    then then then then then then then then then then then 
;

\ select and delete words
: sel-me size @ random 1 + current-b ! ;
: sel-t size @ random 1 + target-b ! ;

: sel-next current-b @ 1 + wrap target-b ! ;
: sel-prev current-b @ 1 - wrap target-b ! ;
: sel-2next current-b @ 2 + wrap target-b ! ;
: sel-2prev current-b @ 2 - wrap target-b ! ;

\ : del-me current-b @ block 1024 erase ;
: del-me size @ 2 + block current-b @ block 1024 cmove ;
: del-t size @ 2 + block target-b @ block 1024 cmove ;
: del-next size @ 2 + block current-b @ 1 + wrap block 1024 cmove ;
: del-prev size @ 2 + block current-b @ 1 - wrap block 1024 cmove ;
\ b-compare compares current blocks first 10 bytes with 10 bytes at end of file
\ compare gives 0 if they are equal (=empty)
: b-compare 
     current-b @ block 10 
     current-b @ block 1000 + 10
     compare
     ;
\ empty? checks if a blockfile is empty, if not, it loads the file
: empty? 
    b-compare 0 = 
    if
	." EMPTY" cr
	\ current-b @ block 1024 erase flush
    else
	current-b @ load
	write
	cr
    then
;
\ b-empty? looks for an empty block to write a file to
: b-empty?
    begin
	sel-me
	b-compare 0 =
    until
;

\ copy-me copies current block to a target    
: copy-me 
    current-b @ block 
    target-b @ block 
    1024 cmove write
;
\ check both neighbours and see who they are!
: neighbour-check-new
    0 prevc ! 0 nextc ! 0 2prevc ! 0 2nextc !
    sel-prev
    target=who? target @ prevc !
    sel-next
    target=who? target @ nextc !
    sel-2prev
    target=who? target @ 2prevc !
    sel-2next
    target=who? target @ 2nextc !
;
\ check for overpopulation
: target=me? current-b @ block 4 target-b @ block 4 compare ;

: overpop?
    0 overpop !
    sel-prev
    target=me?
    0 =
    if
	sel-next
	target=me?
	0 =
	if
	    1 overpop !
	then
    then
;
\ copy-cat: copies itself to the next block
: copy-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted" 
    else
	sel-next
	nextc @ 0 =
	if
	    copy-me
	else
	    sel-prev
	    prevc @ 0 =
	    if
		copy-me
	    then
	then
    then
;
\ eraser-head: erases warlords
: eraser-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted"
    else
	sel-t
	target=who?
	target @ 3 =
	if
	    del-t
	    copy-me
	then
    then	
;
\ anti-emptiness warlord: copies itself to empty slots
: warlord-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted"
    else
	prevc @ 0 =
	if
	    sel-prev
	    copy-me
	then
	2prevc @ 0 =
	if
	    sel-2prev
	    copy-me
	then
	nextc @ 0 =
	if
	    sel-next
	    copy-me
	then
	2nextc @ 0 =
	if
	    sel-2next
	    copy-me
	then
    then
;

\ anti-eraser
: anti-eraser-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted"
    else
	sel-t
	target=who?
	target @ 2 =
	if
	    del-t
	    copy-me
	then
    then
;

\ 3-cell-jumper
: 3-cell-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted"
    else
	prevc @ 6 =
	if
	    sel-prev
	    del-t
	    copy-me
	else
	    current-b @ 3 - wrap target-b !
	    target=who?
	    del-t
	    copy-me
	then
	nextc @ 6 =
	if
	    sel-next
	    del-t
	    copy-me
	else
	    current-b @ 3 - wrap target-b !
	    target=who?
	    del-t
	    copy-me
	then
    then
;

\ 2-cell-jumper
: 2-cell-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted"
    else
	sel-2prev
	del-t
	copy-me
    then
;

\ square-pusher
: square-pusher-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted"
    else
	sel-2next
	del-t 
	sel-next
	target=who?
	target @ 7 =
	if
	    del-me ." deleted myself! "
	else
	    target-b @ block
	    current-b @ 2 + wrap block
	    1024 cmove
	    sel-t
	    del-t
	    copy-me
	    del-me ." deleted !"
	then
    then
;

\ swap-master
: swap-master-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted"
    else
	prevc @ 0 =
	if
	    sel-prev
	    del-t \ test
	    copy-me
	else
\	    nothing
	    current-b @ 5 + wrap target-b !
	    del-t
	    copy-me
	    current-b @ 4 + wrap block
	    current-b @ block 1024 cmove
	then
    then
;

\ destroyer
: destroyer-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted"
    else
	sel-2next
	del-t
	sel-next
	del-t
	sel-2prev
	del-t
	sel-prev
	del-t
	sel-t
	del-t
	copy-me
	del-me
    then
;

\ back-swapper
: back-swapper-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted"
    else
	current-b @ 1 - wrap block
	current-b @ 1 + wrap block
	1024 cmove
	sel-prev
	del-t
	copy-me
	del-me
    then
;

: 32-erase
    current-b @ 15 - wrap target-b !
    31
    for
    del-t
    target-b @ 1 + wrap target-b !
    next
;

\ booby-trap
: booby-trap-ca
    overpop?
    overpop @ 1 =
    if
	del-me ." deleted"
    else
	nextc @ 0 >
	if
	    32-erase
	else
	    prevc @ 0 >
	    if
		32-erase
	    then
	then
    then
;

