\    This file is part of Hello Process.
\
\    Hello Process is free software: you can redistribute it and/or modify
\    it under the terms of the GNU General Public License as published by
\    the Free Software Foundation, either version 3 of the License, or
\    (at your option) any later version.
\
\    Hello Process is distributed in the hope that it will be useful,
\    but WITHOUT ANY WARRANTY; without even the implied warranty of
\    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\    GNU General Public License for more details.
\
\    You should have received a copy of the GNU General Public License
\    along with Hello Process.  If not, see <http://www.gnu.org/licenses/>.

\ urn style PRNG
\
\ provides 2 systems:
\   - urn-dump ( S -- ni..nj )
\     dump on the stack the content of an urn of size S
\   - urn-check ( -- ni )
\     pick one item from the urn and put it on the stack.
\     It can be repeated until the urn is empty.
\     For urn-check to work you need to run urn-init first with desired
\     urn size ( S -- )

include random.fs
here seed !

variable urn 512 cells allot
variable urn-size

\ store i in cell n    -> i n urn-cell !
\ fetch i in cell n    -> n urn-cell @
\ query cell n content -> n urn-cell ?
: urn-cell cells urn + ;

\ fill urn of size S ( S -- )  
: urn-init
    dup
      urn-size !
      0 ?do
	i dup urn-cell !
      loop
;

\ swap cell n1 with cell n2 ( n1 n2 -- )
: cell-swap
    dup urn-cell @
    rot dup urn-cell @
    >r urn-cell !
    r> swap urn-cell !
;

\ fetch i from random cell n of urn of size S ( S --  n i )
: rnd-cell  random dup urn-cell @ ;

\ pick an item out of the urn
: urn-pick
    urn-size @ dup
           >r
	   rnd-cell swap
	r> 1 - cell-swap
    -1 urn-size +!
;

\ dump the urn of size n on stack
: urn-dump
    urn-init
    urn-size @ 0 ?do
	urn-pick
    loop
;

\ manual checkout
: urn-check
    urn-size @ 0 = if
	." urn empty!" cr
    else
	urn-pick
    then
;
