\    This file is part of Hello Process.
\
\    Hello Process is free software: you can redistribute it and/or modify
\    it under the terms of the GNU General Public License as published by
\    the Free Software Foundation, either version 3 of the License, or
\    (at your option) any later version.
\
\    Hello Process is distributed in the hope that it will be useful,
\    but WITHOUT ANY WARRANTY; without even the implied warranty of
\    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\    GNU General Public License for more details.
\
\    You should have received a copy of the GNU General Public License
\    along with Hello Process.  If not, see <http://www.gnu.org/licenses/>.

\ This is the mothership, it controls the execution of the blocks
\ load me using require mothership.fs at the interactive prompt
\ or start gforth with me

\ The mothership loads all needed files and starts the execution of 
\ block 1, after it is executed, it loads block 2, then block 3, etc.
\ The initialisation of the blocks will be a random spreading of x 
\ basic programs over the total y blocks.

\ Init random number generator
require random.fs
here seed !

\ Load block editor and blockfile used to store the programs
use blocked.fb
1 load 2 load 3 load

\ Global variables
variable current-b
variable target-b
variable size
variable speed
variable time
variable print-cycle
variable print-lines
variable total-programs

\ Load options (to set the speed)
: load-cli-arg argc @ 2 arg evaluate speed ! ;

\ Load initialisation files
include urn.fs
include dictionary.fs
include printer.fs

\ Empty word needed for marker call in start
defer forgetwords

\ Main loop
: loopy
    0 ?DO i 1 + dup .
    current-b !  
    empty? 
    LOOP
;

: print-counter print-cycle @ 1 + print-cycle ! ;

: forpause  1000 for next ;

: main
    begin

	\ Draw block line
	draw-blocks

	size @ loopy

	speed @ ms
	\ megapause
	

	
	time @ 1 + time !
	." -------------------" time @ . cr
	print-counter
	print-cycle @ print-lines @ =

    until
;

: initialize!
    S" rm /tmp/fb/hello-process.fb" system

    \ SET VARIABLES HERE: 
    128 size ! 
    0 time ! 
    0 print-cycle !
    40 print-lines !
    11 total-programs !

    S" /tmp/fb/hello-process.fb" open-blocks  \ use hello-process.fb
    size @ 1 + 0 ?DO i block 1024 blank update flush LOOP
    S" initialize-blocks.fs" included \ include initialize-blocks.fs
;

: start
    begin
        forgetwords
	initialize!
	main
    again
;

load-cli-arg
marker forgetwords
start
